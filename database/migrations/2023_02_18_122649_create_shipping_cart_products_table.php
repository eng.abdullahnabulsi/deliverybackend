<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shipping_cart_products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('shipping_cart_id');
            $table->index('shipping_cart_id');

            $table->unsignedBigInteger('product_id');
            $table->index('product_id');

            $table->unsignedBigInteger('order_id');
            $table->index('order_id');

            $table->integer('quntity');
            $table->integer('price');
            $table->string('note');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shipping_cart_products');
    }
};
