<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 100; $i++) {
            $faker = \Faker\Factory::create();

            DB::table('products')->insert([
                'name' => $faker->name,
                'title' => $faker->title,
                'description' => $faker->text,
                'category_id' => rand(1, 4),
                'image_url' => $faker->imageUrl,
                'new_price' => rand(95000, 10800),
                'old_price' => rand(11500, 12500),
            ]);
        }
    }
}
