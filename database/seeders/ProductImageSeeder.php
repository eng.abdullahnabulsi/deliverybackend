<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 1000; $i++) {
            $faker = \Faker\Factory::create();

            DB::table('product_images')->insert([
                'image_path' => $faker->imageUrl,
                'product_id' => rand(1, 1000),
            ]);
        }
    }
}
