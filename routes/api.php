<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\ShippingCartController;
use App\Http\Controllers\ShippingCartOrderController;
use App\Http\Controllers\ShippingCartProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;



// Route::resource('user', UserController::class);

// Route::post('Register', RegisterController::class);

Route::controller(UserController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    // Route::post('logout', 'logout');
    // Route::post('refresh', 'refresh');
});





Route::resource('product', ProductController::class);
Route::resource('category', CategoryController::class);
Route::resource('order', OrderController::class);


Route::prefix('saddon')->middleware('auth:sanctum')->group(function () {
    Route::post('register', 'App\Http\Controllers\UserController@index');
    Route::resource('shipment', ShipmentController::class);
    Route::resource('shipping-cart-order', ShippingCartOrderController::class);
    Route::resource('shipping-cart-product', ShippingCartProductController::class);
    Route::resource('shipping-cart', ShippingCartController::class);
    Route::resource('shipping-cart', ShippingCartController::class);
});
