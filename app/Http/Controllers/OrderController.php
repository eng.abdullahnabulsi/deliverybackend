<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\ShippingCartProduct;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Order::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //Create Order ID
        $order = new Order();
        $order->total_amount = 0;
        $order->save();
        //Assigne ShippingCartProduct
        $total = 0;

        $model = $request->data;
        foreach ($model as $item) {
            $scp = new ShippingCartProduct();
            $scp->quntity = $item['quantity'];
            $scp->price = $item['new_price'];
            $scp->shipping_cart_id = $request->user_cart_id;
            $scp->note = $item['note'];
            $scp->order_id = $order['id'];
            $scp->product_id = $item['id'];
            $total = $total + ($scp->quntity * $scp->price);
            $scp->save();
        }
        $order->total_amount = $total;
        $order->save();
        return ShippingCartProduct::where('order_id', $order['id'])->get();
        // return $request->data;

    }

    /**
     * Display the specified resource.
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Order $order)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order)
    {
        $model = Order::find($order->id)->delete();
        return $model;
    }
}
