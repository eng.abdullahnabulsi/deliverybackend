<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return
            Product::when($request->id, function ($query, $id) {
                $query->find($id);
            })
            ->when($request->categoryid, function ($query, $categoryid) {
                $query->where('category_id', $categoryid);
            })
            ->paginate(200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $product = Product::create([
            'name' => $request->name,
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'old_price' => $request->old_price,
            'new_price' => $request->new_price,
        ]);

        $product->save();


        foreach ($request->image_path as $images => $image) {
            $productimage = ProductImage::create([
                'image_path' => $image,
                'product_id' => $product->id
            ]);

            $productimage->save();
        }

        return $product;
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {

        $product = Product::where('id', $product->id)
            ->update([
                'name' => $request->input('name'),
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'category_id' => $request->input('category_id'),
                'old_price' => $request->old_price,
                'new_price' => $request->new_price,
            ]);


        return $product;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $model = Product::find($product->id);
        $model->delete();
        return $model;
    }
}
