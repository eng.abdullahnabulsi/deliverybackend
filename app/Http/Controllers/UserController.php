<?php

namespace App\Http\Controllers;

use App\Models\ShippingCart;
use App\Models\User;
use Illuminate\Http\Request;



use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use function PHPUnit\Framework\isEmpty;

class UserController extends Controller
{

    public function index(Request $request)
    {
        //order desc or asc
        return User::select('name', 'email', 'password')
            ->when($request->name, function ($query, string $name) {
                $query->where('name', 'LIKE', '%' . $name . '%');
            })
            ->when($request->id, function ($query, string $id) {
                $query->find($id);
            })
            ->orderBy("name", $request->order_by ? $request->order_by : "desc")
            ->paginate($request->per_page  ? $request->per_page : 15);
    }


    public function login(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                $user = auth()->user();
                $token =  $user->createToken('token')->plainTextToken;
                $cart = ShippingCart::where('user_id', $user->id)->get();
                return  response()->json([
                    'status' => 'succes',
                    'token' => $token,
                    'data' => $user,
                    'cart' => $cart[0]->id
                ]);
            } else {
                return response()->json([
                    'status' => 'false',
                    'message' => 'error'
                ]);
            }
        } catch (\Throwable $th) {
            response()->json([
                'status' => 'false',
                'message' => 'error'
            ]);
        }
    }


    public function register(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => 'required|unique:users',
                'password' => 'required'
            ]);


            $user = User::create(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => Hash::make($request->password),
                ]
            );
            $user->save();

            $cart = ShippingCart::create([
                'user_id' => $user->id
            ]);
            $cart->save();
            $token =  $user->createToken('token')->plainTextToken;


            return response()->json([
                'status' => 'succes',
                'token' =>  $token,
                'data' => $user,
                'cart' => $cart->id
            ]);
        } catch (\Throwable $th) {
            return
                response()->json([
                    'status' => 'false',
                    'message' => $th
                ]);
        }
    }

    public function update(Request $request, User $user)
    {
        try {
            User::where('id', $user->id)->update(
                [
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'password' => Hash::make($request->input('password')),
                ]
            );
            return response()->json([
                'status' => 'succes',
                'message' => 'Object has been update successfuly',
                'data' =>  User::find('id', $user->id)
            ]);
        } catch (\Throwable $th) {
            return
                response()->json([
                    'status' => 'false',
                    'message' => 'error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        try {
            $model = User::find($user->id);
            if ($model) {
                $model->delete();
                return response()->json([
                    'status' => 'succes',
                    'data' =>  'User Has been deleted successfuly'
                ]);
            } else {
                return
                    response()->json([
                        'status' => 'false',
                        'message' => 'error'
                    ]);
            }
        } catch (\Throwable $th) {
            return
                response()->json([
                    'status' => 'false',
                    'message' => 'error'
                ]);
        }
    }
}
