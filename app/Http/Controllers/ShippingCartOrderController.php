<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\ShippingCartOrder;
use App\Models\ShippingCartProduct;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ShippingCartOrderController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ShippingCartOrder::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $model = ShippingCartProduct::select('quntity', 'price', 'product_id')
            ->where('shipping_cart_id', $request->id)->get();

        $total_amount = 0;
        foreach ($model as $key => $value) {
            $total_amount += ($value->price * $value->quntity);
        }

        $order = Order::create([
            'total_amount' => $total_amount
        ]);

        $modelshippingcartorder = ShippingCartOrder::create([
            'shipping_cart_id' => $request->id,
            'order_id' => $order->id
        ]);

        return $modelshippingcartorder;
    }

    /**
     * Display the specified resource.
     */
    public function show(ShippingCartOrder $shippingCartOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ShippingCartOrder $shippingCartOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ShippingCartOrder $shippingCartOrder)
    {
        $model = ShippingCartProduct::select('quntity', 'price', 'product_id')
            ->where('shipping_cart_id', $request->shipping_cart_id)->get();

        $total_amount = 0;
        foreach ($model as $key => $value) {
            $total_amount += ($value->price * $value->quntity);
        }

        $order = Order::find($request->order_id)->update([
            'total_amount' => $total_amount
        ]);

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ShippingCartOrder $shippingCartOrder)
    {
    }
}
