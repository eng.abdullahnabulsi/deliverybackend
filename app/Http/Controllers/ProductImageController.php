<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductImageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        return ProductImage::select('image_path')
            ->with('productimages')
            ->get();
        // ->when($request->id, function ($query, $id) {
        //     $query->where('images.product_id', $id);
        // })
        // ->orderBy('new_price', $request->order_by ? $request->order_by : "desc")
        // ->paginate(15);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $model = $request->validate([
            'new_price' => 'required|integer'
        ]);
        if ($model) {
            $price = Price::create($request->all());
            $price->save();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Price $price)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Price $price)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Price $price)
    {
        //
    }
}
