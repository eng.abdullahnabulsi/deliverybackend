<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        //order desc or asc

        return Category::paginate(15);
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|unique:categories',
        ]);

        if ($validate) {
            $model = Category::create($request->all());
            $model->save();
            return response()->json([
                'status' => 'success',
                'data' => $model
            ]);
        } else {
            return
                response()->json([
                    'status' => 'success',
                    'message' => 'error'
                ]);
        }
        return
            response()->json([
                'status' => 'success',
                'message' => 'error'
            ]);
    }

    public function update(Request $request, User $user)
    {
        $model = User::where('id', $user->id)->update(
            [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ]
        );
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $model = User::find($user->id);
        $model->delete();
        return $model;
    }
}
