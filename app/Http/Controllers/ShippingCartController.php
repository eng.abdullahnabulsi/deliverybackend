<?php

namespace App\Http\Controllers;

use App\Models\ShippingCart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ShippingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ShippingCart::paginate(200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'user_id' => 'required'
        ]);

    }

    /**
     * Display the specified resource.
     */
    public function show(ShippingCart $shippingCart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ShippingCart $shippingCart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ShippingCart $shippingCart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ShippingCart $shippingCart)
    {
        //
    }
}
