<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\ShippingCart;
use App\Models\ShippingCartProduct;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ShippingCartProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ShippingCartProduct::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'quntity',
            'price',
            'note',
            'shipping_cart_id',
            'product_id',

        ]);
        $model = ShippingCartProduct::create($request->all());
        $model->save();

        return $model;
    }

    /**
     * Display the specified resource.
     */
    public function show(ShippingCartProduct $shippingCartProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ShippingCartProduct $shippingCartProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ShippingCartProduct $shippingCartProduct)
    {
        $validate = $request->validate([
            'quntity',
            'price',
            'note',
            'shipping_cart_id',
            'product_id',

        ]);
        $model = ShippingCartProduct::finde($shippingCartProduct->id)
            ->update([
                'quntity' => $request->input('quntity'),
                'price' =>  $request->input('price'),
                'note' => $request->input('note')
            ]);

            


        return $model;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ShippingCartProduct $shippingCartProduct)
    {
        $model = ShippingCartProduct::find($shippingCartProduct->id)->delete();
        return $model;
    }
}
