<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCartOrder extends Model
{
    use HasFactory;


    protected $fillable = ['shipping_cart_id', 'order_id'];
}
