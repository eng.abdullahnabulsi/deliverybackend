<?php

class ProductCartItem
{

    protected $fillable = [
        'name',
        'title',
        'newPrice',
        'oldPrice',
        'description',
        'imageUrl',
        'deleteAt',
        'createdAt',
        'updatedAt',
        'currencyType',
        'quantity'
    ];
}
